#!/bin/bash
  
# Stop display manager
#systemctl stop sddm.service
pkill X
  
# Unbind VTconsoles
echo 0 | sudo tee /sys/class/vtconsole/vtcon0/bind
echo 0 | sudo tee /sys/class/vtconsole/vtcon1/bind
  
# Unbind EFI-Framebuffer
echo efi-framebuffer.0 | sudo tee /sys/bus/platform/drivers/efi-framebuffer/unbind

# Unbind the GPU from display driver
#echo -n "0000:23:00.0" > /sys/bus/pci/drivers/nvidia/unbind
#echo -n "0000:23:00.1" > /sys/bus/pci/drivers/snd_hda_intel/unbind

# Unbind the GPU from display driver

## Load the config file
source "/etc/libvirt/hooks/kvm.conf"

virsh nodedev-detach $VIRSH_GPU_VIDEO
virsh nodedev-detach $VIRSH_GPU_AUDIO

#virsh nodedev-detach pci_0000_1C_00_0
#virsh nodedev-detach pci_0000_1C_00_1

# Load VFIO Kernel Module  
modprobe vfio
modprobe vfio_iommu_type1
modprobe vfio-pci


sleep 1
