#!/bin/bash

# Unload VFIO-PCI Kernel Driver
modprobe -r vfio-pci
modprobe -r vfio_iommu_type1
modprobe -r vfio
  
# Re-Bind GPU to Nvidia Driver
#echo -n "0000:23:00.0" > /sys/bus/pci/drivers/nvidia/bind
#echo -n "0000:23:00.1" > /sys/bus/pci/drivers/snd_hda_intel/bind
# Re-Bind GPU to Nvidia Driver

## Load the config file
source "/etc/libvirt/hooks/kvm.conf"
virsh nodedev-reattach $VIRSH_GPU_VIDEO
virsh nodedev-reattach $VIRSH_GPU_AUDIO

#virsh nodedev-reattach pci_0000_1C_00_1
#virsh nodedev-reattach pci_0000_1C_00_0

# Wait 1 second to avoid possible race condition
sleep 1
  
# Re-Bind EFI-Framebuffer
nvidia-xconfig --query-gpu-info > /dev/null 2>&1

echo efi-framebuffer.0 | sudo tee /sys/bus/platform/drivers/efi-framebuffer/bind
  
# Re-bind to virtual consoles
echo 1 | sudo tee /sys/class/vtconsole/vtcon0/bind
echo 1 | sudo tee /sys/class/vtconsole/vtcon1/bind

sleep 1
# Restart Display Manager
#systemctl start sddm.service

